--
-- PostgreSQL database dump
--

-- Dumped from database version 10.5
-- Dumped by pg_dump version 10.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: akun_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.akun_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.akun_seq OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: akun; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.akun (
    id integer DEFAULT nextval('public.akun_seq'::regclass) NOT NULL,
    nik character varying(255) NOT NULL,
    password character varying(255) NOT NULL,
    nama character varying(255) NOT NULL,
    tempat_lahir character varying(255) NOT NULL,
    tanggal_lahir date,
    email character varying(255) NOT NULL,
    grup_id integer NOT NULL,
    jenis_kelamin character(1),
    api_key character varying(255)
);


ALTER TABLE public.akun OWNER TO postgres;

--
-- Name: desa_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.desa_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.desa_seq OWNER TO postgres;

--
-- Name: desa; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.desa (
    id integer DEFAULT nextval('public.desa_seq'::regclass) NOT NULL,
    nama_desa character varying(255) NOT NULL
);


ALTER TABLE public.desa OWNER TO postgres;

--
-- Name: grup_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.grup_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.grup_seq OWNER TO postgres;

--
-- Name: grup; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.grup (
    id integer DEFAULT nextval('public.grup_seq'::regclass) NOT NULL,
    nama_grup character varying(255) NOT NULL,
    keterangan character varying(255)
);


ALTER TABLE public.grup OWNER TO postgres;

--
-- Name: jenis_pengaduan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.jenis_pengaduan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.jenis_pengaduan_seq OWNER TO postgres;

--
-- Name: jenis_pengaduan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.jenis_pengaduan (
    id integer DEFAULT nextval('public.jenis_pengaduan_seq'::regclass) NOT NULL,
    nama_jenis character varying(255) NOT NULL,
    keterangan_jenis character varying(255)
);


ALTER TABLE public.jenis_pengaduan OWNER TO postgres;

--
-- Name: pengaduan_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pengaduan_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pengaduan_seq OWNER TO postgres;

--
-- Name: pengaduan; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pengaduan (
    id integer DEFAULT nextval('public.pengaduan_seq'::regclass) NOT NULL,
    lg character varying(255),
    lt character varying(255),
    alamat_lokasi character varying(255) NOT NULL,
    jenispengaduan_id integer NOT NULL,
    desa_id integer NOT NULL,
    akun_id integer NOT NULL,
    isi_pengaduan text NOT NULL,
    tanggapan text,
    status_id integer,
    photo character varying(255),
    waktu timestamp with time zone
);


ALTER TABLE public.pengaduan OWNER TO postgres;

--
-- Name: riwayat_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.riwayat_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.riwayat_seq OWNER TO postgres;

--
-- Name: riwayat; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.riwayat (
    id integer DEFAULT nextval('public.riwayat_seq'::regclass) NOT NULL,
    keterangan character varying(255) NOT NULL,
    pengaduan_id integer NOT NULL,
    tanggal_dibuat timestamp without time zone,
    created_by integer
);


ALTER TABLE public.riwayat OWNER TO postgres;

--
-- Name: status_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.status_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.status_seq OWNER TO postgres;

--
-- Name: status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.status (
    id integer DEFAULT nextval('public.status_seq'::regclass) NOT NULL,
    nama_status character varying(255) NOT NULL,
    keterangan character varying(255)
);


ALTER TABLE public.status OWNER TO postgres;

--
-- Data for Name: akun; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.akun (id, nik, password, nama, tempat_lahir, tanggal_lahir, email, grup_id, jenis_kelamin, api_key) FROM stdin;
3	1234567890	e807f1fcf82d132f9bb018ca6738a19f	Intan F	Kenton	2019-02-05	intan@mail.com	3	P	5c726d1bf168b
6	4567	827ccb0eea8a706c4c34a16891f84e7b	www	kediri	2019-01-30	www@gmail.com	4	L	\N
8	2345	31cf2b3561b2aed60bf8c02414cc955a	frans	kediri	2019-01-29	frans@gmail.com	4	P	\N
9	1461600137	b59c67bf196a4758191e42f76670ceba	Arif Nehru	Papar	1997-04-17	arif@gmail.com	2	L	\N
10	1461600118	b59c67bf196a4758191e42f76670ceba	Nuzulia Rahma	Porong	1998-01-16	nuzulia@gmail.com	5	P	\N
11	165784920	b59c67bf196a4758191e42f76670ceba	Rafi Eko	Kediri	1997-11-20	rafi@gmail.com	6	L	\N
12	1456789	b59c67bf196a4758191e42f76670ceba	Bambang Budiono	Jakarta	1995-08-02	bambangbudiono@gmail.com	7	L	\N
2	9876543210	f3a0a79aebcb8c9534efe7280abc8047	Febri Ahmad	Papar	2019-02-15	febri@mail.com	8	L	d41d8cd98f00b204e9800998ecf8427e
\.


--
-- Data for Name: desa; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.desa (id, nama_desa) FROM stdin;
6	Grogol
1	Ngadisimo Utara
11	desakuuu
17	djadhjadjadj
\.


--
-- Data for Name: grup; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.grup (id, nama_grup, keterangan) FROM stdin;
3	Insfratruktur	jalanan
4	Warga	\N
2	Sarana dan Prasarana	Sarana dan Prasarana
5	Kesehatan	Kesehatan
6	Pelayanan	Pengaduan Pelayanan masyarakat
7	Verifikasi	Verifikasi
8	Super User	\N
\.


--
-- Data for Name: jenis_pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.jenis_pengaduan (id, nama_jenis, keterangan_jenis) FROM stdin;
3	Kesehatan	kesehatan
6	Infrastruktur	
8	Sarana Dan Prasarana	
9	Pelayanan	
\.


--
-- Data for Name: pengaduan; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.pengaduan (id, lg, lt, alamat_lokasi, jenispengaduan_id, desa_id, akun_id, isi_pengaduan, tanggapan, status_id, photo, waktu) FROM stdin;
17	\N	\N	KEDIRI	6	1	3	SDHHFHSFHAFBHSBF		1	5c750008274fe.jpg	2019-03-02 19:17:47+07
21	-7.2432	112.7413	askdhkasd	3	6	8	Hellow World		1	5c765d45cd64e.jpg	2019-03-13 19:17:54+07
22	-7.242476260520763	112.7439607513428	hghjgj	3	6	8	ghjgjgjahdad		1	5c768d5f88000.jpg	2019-02-12 19:18:00+07
18	\N	\N	wjjw	8	11	3	hweww dwdhw	jhjkdhkajshdkj asdjhasd jhaskjdhas asdjhasd akdsjhasd asdha asjdhasd asdjhasd 	3	5c7500aee3c4c.jpg	2019-02-12 19:18:08+07
\.


--
-- Data for Name: riwayat; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.riwayat (id, keterangan, pengaduan_id, tanggal_dibuat, created_by) FROM stdin;
1	Pengaduan dibuat.	21	2019-02-27 10:49:58	8
2	Pengaduan dibuat.	22	2019-02-27 02:15:11	8
3	Pengaduan dibuat.	18	2019-02-28 12:09:16	9
4	Pengaduan dibuat.	18	2019-02-28 12:41:11	9
5	Pengaduan selesai diproses.	18	2019-03-01 09:24:38	12
\.


--
-- Data for Name: status; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.status (id, nama_status, keterangan) FROM stdin;
1	belum diproses	\N
2	sedang diproses	\N
3	selesai	\N
4	archive	\N
\.


--
-- Name: akun_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.akun_seq', 12, true);


--
-- Name: desa_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.desa_seq', 17, true);


--
-- Name: grup_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.grup_seq', 8, true);


--
-- Name: jenis_pengaduan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.jenis_pengaduan_seq', 10, true);


--
-- Name: pengaduan_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pengaduan_seq', 22, true);


--
-- Name: riwayat_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.riwayat_seq', 5, true);


--
-- Name: status_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.status_seq', 3, true);


--
-- Name: akun akun_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.akun
    ADD CONSTRAINT akun_pkey PRIMARY KEY (id);


--
-- Name: desa desa_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.desa
    ADD CONSTRAINT desa_pkey PRIMARY KEY (id);


--
-- Name: grup grup_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.grup
    ADD CONSTRAINT grup_pkey PRIMARY KEY (id);


--
-- Name: jenis_pengaduan jenis_pengaduan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.jenis_pengaduan
    ADD CONSTRAINT jenis_pengaduan_pkey PRIMARY KEY (id);


--
-- Name: pengaduan pengaduan_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pengaduan
    ADD CONSTRAINT pengaduan_pkey PRIMARY KEY (id);


--
-- Name: riwayat riwayat_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.riwayat
    ADD CONSTRAINT riwayat_pkey PRIMARY KEY (id);


--
-- Name: status status_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (id);


--
-- PostgreSQL database dump complete
--

