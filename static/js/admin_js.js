$(document).ready(function(){
    $("body").attr("data-ma-theme", storage.color);
    html_color = '<label class="btn bg-green" onclick="color_change()"><input type="radio" value="green" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-blue" onclick="color_change()"><input type="radio" value="blue" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-red" onclick="color_change()"><input type="radio" value="red" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-orange" onclick="color_change()"><input type="radio" value="orange" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-teal" onclick="color_change()"><input type="radio" value="teal" autocomplete="off" name="color_input"></label>'+
    '<div class="clearfix mt-2"></div>'+
    '<label class="btn bg-cyan" onclick="color_change()"><input type="radio" value="cyan" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-blue-grey" onclick="color_change()"><input type="radio" value="blue-grey" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-purple" onclick="color_change()"><input type="radio" value="purple" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-indigo" onclick="color_change()"><input type="radio" value="indigo" autocomplete="off" name="color_input"></label>'+
    '<label class="btn bg-brown" onclick="color_change()"><input type="radio" value="brown" autocomplete="off" name="color_input"></label>';
    $(".btn-group--colors").html(html_color);
})

var color_change = function(){
    setTimeout(function () {
        color = $("body").attr("data-ma-theme");
        storage.setItem("color", color)
    },1000);
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};


function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
    
function changeUrl(key,value) {
    //Get query string value
    var searchUrl=location.search;
    if(searchUrl.indexOf("?")== "-1") {
        var urlValue='?'+key+'='+value;
        history.pushState({state:1, rand: Math.random()}, '', urlValue);
    }else{
        //Check for key in query string, if not present
        if(searchUrl.indexOf(key)== "-1") {
            var urlValue=searchUrl+'&'+key+'='+value;
        }
        else {  //If key present in query string
            oldValue = getParameterByName(key);
            if(searchUrl.indexOf("?"+key+"=")!= "-1") {
                urlValue = searchUrl.replace('?'+key+'='+oldValue,'?'+key+'='+value);
            }
            else {
                urlValue = searchUrl.replace('&'+key+'='+oldValue,'&'+key+'='+value); 
            }
        }
        history.pushState({state:1, rand: Math.random()}, '', urlValue);
        //history.pushState function is used to add history state.
        //It takes three parameters: a state object, a title (which is currently ignored), and (optionally) a URL.
    }
}