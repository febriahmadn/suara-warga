<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'welcome';

$route['admin'] = 'admin/home';
$route['add'] = 'welcome/add';
$route['login'] = 'welcome/login';
$route['addaction'] = 'welcome/addaction';
$route['daftar'] = 'welcome/daftar';
$route['daftaraction'] = 'welcome/daftar_action';
$route['actionlogin'] = 'welcome/actionlogin';
$route['logout'] = 'welcome/logout';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;