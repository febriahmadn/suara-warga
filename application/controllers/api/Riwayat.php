<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Riwayat extends CI_Controller {
	function __construct(){
		parent:: __construct();
		header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    	$this->load->model('Model_riwayat', 'model');
    	$this->load->model('Model_akun', 'model_akun');
    	
    }

    public function index(){
		$id = NULL;
		$pengaduan_id = NULL;
		$msg['success'] = false;
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if(isset($_REQUEST['pengaduan_id'])) $pengaduan_id = $_REQUEST['pengaduan_id'];
		if($this->model_akun->cek_api() == TRUE){
			$msg['success'] = true;
			$result = $this->model->getAll($pengaduan_id);
			if($result){
				$msg['success'] = true;
				$msg['status'] = 200;
				$msg['data'] = $result;
				if($id != NULL){
					$msg['data'] = $result[0];
				}
			}
		}
		echo json_encode($msg);
	}
}