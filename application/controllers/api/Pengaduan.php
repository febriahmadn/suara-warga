<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {
	function __construct(){
		parent:: __construct();
		header('Access-Control-Allow-Origin: *');
    	header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    	$this->load->model('Model_pengaduan', 'model');
    	$this->load->model('Model_akun', 'model_akun');
	}

	public function index(){
		$id = NULL;
		$msg['success'] = false;
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if($this->model_akun->cek_api() == TRUE){
			$msg['success'] = true;
			$result = $this->model->getAll();
			if($result){
				$msg['success'] = true;
				$msg['status'] = 200;
				$msg['data'] = $result;
				if($id != NULL){
					$msg['data'] = $result[0];
				}
			}
		}
		echo json_encode($msg);
	}

	// fungsi untuk meresize gambar
	function resizeImage($filename){
		$source_path = './media/' . $filename;
		$target_path = './media/thumbnail/';
		$config_manip = array(
			'image_library' => 'gd2',
			'source_image' => $source_path,
			'new_image' => $target_path,
			'maintain_ratio' => TRUE,
			'width' => 150,
			'height' => 150
		);

		$this->load->library('image_lib', $config_manip);
		if(!$this->image_lib->resize()){
			echo $this->image_lib->display_errors();
		}
		$this->image_lib->clear();
	}

	// fungsi untuk upload image
	function _uploadimage(){
		$config['upload_path'] = "./media";
		$config['allowed_types'] = "jpg|jpeg|png";
		$config['file_name'] = uniqid();
		$config['overwrite'] = true;
		$config['max_size'] = 3000;
		$this->load->library('upload', $config);
		if($this->upload->do_upload('image_file')){
			$this->resizeImage($this->upload->data('file_name'));
			return $this->upload->data('file_name');
		}
		return "default.png";
	}

	public function add(){
		$data['success'] = false;
		if($this->model_akun->cek_api() == TRUE){
			$file = $this->_uploadimage();
			$query = $this->model->add($file);
			if($query == true){
				$data['success'] = true;
			}
		}
		echo json_encode($data);
	}

}