<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaduan extends CI_Controller {
	function __construct(){
		parent:: __construct();
		$this->load->model('Model_pengaduan', 'model');
		$this->load->library('form_validation');
		if(!$this->session->userdata('logged_in')){
			redirect(base_url("admin/login"), "refresh");
		}
	}

	public function index(){
		$data['title'] = "Pengaduan";
		$data['menu_active'] = "pengaduan";
		$this->load->view('admin/pengaduan', $data);
	}

	public function pengaduansudahtertangani(){
		$data['title'] = "Pengaduan Sudah Tertangani";
		$data['menu_active'] = "sudahpengaduan";
		$data['url'] = base_url('admin/pengaduan');
		$this->load->view('admin/pengaduan', $data);
	}

	function cetak_pdf(){

		$this->db->select('pengaduan.photo, pengaduan.id, pengaduan.waktu, pengaduan.lg, pengaduan.lt, pengaduan.alamat_lokasi, pengaduan.isi_pengaduan, pengaduan.tanggapan, pengaduan.jenispengaduan_id, pengaduan.desa_id, pengaduan.akun_id, pengaduan.status_id, jenis_pengaduan.nama_jenis, desa.nama_desa, akun.nama, status.nama_status');
		
		$this->db->from('pengaduan');
		$this->db->join('jenis_pengaduan', 'pengaduan.jenispengaduan_id=jenis_pengaduan.id', 'left');
		$this->db->join('desa', 'pengaduan.desa_id=desa.id', 'left');
		$this->db->join('akun', 'pengaduan.akun_id=akun.id', 'left');
		$this->db->join('status', 'pengaduan.status_id=status.id', 'left');
		$query = $this->db->get();
		$data['result'] = $query->result();

		$this->load->view('admin/cetak', $data);
		$html = $this->output->get_output();
		$this->load->library('dompdf_gen');
		$this->dompdf->load_html($html, 'a4', 'landscape');
		$this->dompdf->render();
		$this->dompdf->stream("cetak_pinjam_barang.pdf",array('Attachment'=>0));
	}

	public function show(){
		$limit = 20;
		$start = 0;
		$next = 20;
		$previous = 0;
		$q = "";
		$id = NULL;
		$p = NULL;

		if(isset($_REQUEST['limit'])) $limit = $_REQUEST['limit'];
		if(isset($_REQUEST['start'])) $start = $_REQUEST['start'];
		if(isset($_REQUEST['q'])) $q = $_REQUEST['q'];
		if(isset($_REQUEST['id'])) $id = $_REQUEST['id'];
		if(isset($_REQUEST['p'])) $p = $_REQUEST['p'];

		$result = $this->model->getlist($limit, $start, $q, $id, $p);
		$total_count_page = count($result);

		$total_count = $this->model->get_count($q);
		$total_pagination = $total_count / $limit;
		$total_pagination = (int)$total_pagination;
		if ($total_count % $limit > 0){
			$total_pagination = $total_pagination + 1;
		}
		$link_next = "";
		if(count($result) == $limit){
			$link_next = "/admin/show?limit=".$limit."&start=".$next;
		}

		$link_prev = "";
		if($start != 0){
			$link_prev = "/admin/show?limit=".$limit."&start=".$previous;
		}

		if((int)$start != 0){
			$next = (int)$start+20;
			$previous = (int)$start-20;
		}
		$meta = array(
			'start' => $start,
			'limit' => $limit,
			'next' => $link_next,
			'previous' => $link_prev,
			'total_count' => $total_count,
			'total_pagination' => $total_pagination,
			'total_count_page' => $total_count_page,
		);

		$data = array(
			'meta' => $meta,
			'objects' => $result,
		);
		if(isset($_REQUEST['id'])){
			$data = $result[0];
		}
		echo json_encode($data);
	}

	function getall(){
		$data = $this->model->getAll();
		echo json_encode($data);
	}

	function actiontanggapi(){
		$this->form_validation->set_rules('isi_tanggapan', 'Isi Tanggapan', 'required');
		$data['success'] = false;
		if($this->form_validation->run() != false){
			$userdata = $this->session->userdata('result');
			$query = $this->model->simpan_tanggapi($userdata->id);
			if($query == true){
				$data['success'] = true;
			}
		}else{	
			$data['error'] = validation_errors();
		}
		echo json_encode($data);
	}

	function varifikasifinish($id){
		$data['success'] = false;
		$userdata = $this->session->userdata('result');
		$query = $this->model->varifikasifinish($id, $userdata->id);
		if($query == true){
			$data['success'] = true;
		}
		echo json_encode($data);
	}

	function jumlahpengaduanbulanan(){
		$data = $this->model->jumlahpengaduanbulanan();
		echo json_encode($data);
	}

	function getstatistik(){
		$data = array();
		$this->db->select('nama_jenis, id');
		$this->db->from('jenis_pengaduan');
		$query = $this->db->get();
		$jenis_pengaduan = $query->result();

		foreach($jenis_pengaduan as $value) {
			$databulanan = [];
			for ($x = 0; $x < 12; $x++) {
				$this->db->from('pengaduan');
    			$this->db->where('jenispengaduan_id', $value->id);
    			$this->db->where("EXTRACT(MONTH FROM waktu) = ", $x+1);
    			$jumlah = $this->db->count_all_results();
				$databulanan_ = [$x+1,  $jumlah];
				array_push($databulanan, $databulanan_);
			}
			$data_ = array(
				'nama_jenis' => $value->nama_jenis,
				'data' => $databulanan
			);
			array_push($data, $data_);
		}
		
		echo json_encode($data);
	}
}