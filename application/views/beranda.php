<?php include('base.php') ?>

<?php startblock('title') ?>
    Beranda
<?php endblock() ?>

<?php startblock('content') ?>
    <div class="row">
        <div class="col-md-12 change-list-beranda">
        </div>
    </div>
    <?php if($user_saya){ ?>
        <a href="<?= base_url() ?>add" type="button" class="btn btn-primary float">Tambah Pengaduan</a>
    <?php } ?>
<?php endblock() ?>

<?php startblock('js') ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY8xmhot-viwVybAxaOD_Qissx0wCJUcA"></script>
<?php endblock() ?>

<?php startblock('custom_js') ?>
    <script type="text/javascript">
        $(function(){
            $.ajax({
                url: "<?= base_url() ?>welcome/show",
                type: "GET",
                success: function(respon){
                    respon = JSON.parse(respon);
                    if(respon){
                        for (var i = 0; i < respon.objects.length; i++){
                            obj_ = respon.objects[i];
                            tanggapan_ = '';
                            if(obj_.tanggapan){
                                tanggapan_ = '<div class="col-md-6">'+
                                            '<div class="card bg-black card--inverse">'+
                                                '<div class="card-body">'+
                                                    '<h4 class="card-title">Tanggapan Pengaduan</h4>'+
                                                    '<p class="card-text detil-isi_pengaduan">'+obj_.tanggapan+'</p>'+
                                                '</div>'+
                                            '</div>'+
                                        '</div>';
                            }
                            

                            html_ = '<div class="card">'+
                                        '<div class="card-body">'+
                                            '<div class="actions">'+
                                                '<span class="badge badge-primary">'+obj_.nama_status+'</span>'+
                                            '</div>'+
                                            '<h4 class="card-title">'+
                                                '<img src="<?= base_url() ?>static/admin/demo/img/profile-pics/1.jpg" class="listview__img" alt="">'+obj_.nama+'</h4>'+
                                            '<h6 class="card-subtitle">'+obj_.nama_jenis+' | '+obj_.waktu+' | '+obj_.alamat_lokasi+'</h6>'+
                                            '<div class="row">'+
                                                '<div class="col-md-4">'+
                                                    '<div class="row lightbox photos">'+
                                                        '<a href="<?= base_url() ?>media/'+obj_.photo+'" class="col-md-12">'+
                                                            '<div class="lightbox__item photos__item">'+
                                                                '<img src="<?= base_url() ?>media/thumbnail/'+obj_.photo+'" alt="" />'+
                                                            '</div>'+
                                                        '</a>'+
                                                    '</div>'+
                                                '</div>'+
                                                '<div class="col-md-8">'+
                                                    '<div id="googleMap-'+i+'" style="width:100%;height:300px;"></div>'+
                                                '</div>'+
                                                '<div class="col-md-6">'+
                                                    '<div class="card bg-green card--inverse">'+
                                                        '<div class="card-body">'+
                                                            '<h4 class="card-title">Isi Pengaduan</h4>'+
                                                            '<p class="card-text detil-isi_pengaduan">'+obj_.isi_pengaduan+'</p>'+
                                                        '</div>'+
                                                    '</div>'+
                                                '</div>'+
                                                tanggapan_+
                                            '</div>'+
                                        '</div>'+
                                    '</div>';
                            $('.change-list-beranda').append(html_);
                            if(obj_.lt && obj_.lg){
                                petagoogle(obj_.lg, obj_.lt, 'googleMap-'+i);
                            }
                        }
                        $('.lightbox').lightGallery({
                            thumbnail:true,
                            animateThumb: false,
                            showThumbByDefault: false
                        });
                    }
                }
            })
        });

        var petagoogle = function(lg, lt, elem){
            var myLatLng = {lat: Number(lg), lng: Number(lt)};
            var mapProp = new google.maps.Map(document.getElementById(elem), {
                zoom: 12,
                center: myLatLng
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: mapProp,
            });
        }
    </script>
<?php endblock() ?>
