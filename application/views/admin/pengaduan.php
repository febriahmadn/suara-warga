<?php include('change_list.php') ?>

<?php startblock('header') ?>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?= base_url() ?>admin">Home</a></li>
		<li class="breadcrumb-item"><a href="<?= base_url() ?>admin/pengaduan">Pengaduan</a></li>
		<li class="breadcrumb-item active">Data <?= $title ?></li>
	</ol>
<?php endblock() ?>

<?php startblock('change_list__action') ?>
	<a href="<?= base_url() ?>admin/pengaduan/cetak_pdf">
		<button type="button" class="btn btn-success">Cetak PDF</button>
	</a>
<?php endblock() ?>

<?php startblock('change_list') ?>
	<table class="table table-striped mb-0 change-list">
		<thead>
			<tr>
				<th>No.</th>
				<th>Pengadu</th>
				<th>Tanggal & Waktu</th>
				<!-- <th>Lokasi</th> -->
				<th>Isi Pengaduan</th>
				<th>Tanggapan</th>
				<th>Jenis Pengaduan</th>
				<!-- <th>Desa </th> -->
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
<?php endblock() ?>

<?php startblock('js') ?>
	<?php superblock() ?>
	<script type="text/javascript" src="<?= base_url() ?>static/js/dotdotdot.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAY8xmhot-viwVybAxaOD_Qissx0wCJUcA"></script>
	<div class="modal fade" id="modal-pengaduan_map" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pull-left">Map</h5>
				</div>
				<div class="modal-body">
					<div id="googleMap" style="width:100%;height:400px;"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="modal-default" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pull-left">Detail Pengaduan</h5>
				</div>
				<div class="modal-body">
					<table class="table table-striped">
						<tr>
							<td>Nama Pengadu</td>
							<td>:</td>
							<td><span class="detil-nama"></span></td>
						</tr>
						<tr>
							<td>Jenis Pengaduan</td>
							<td>:</td>
							<td><span class="detil-jenis_pengaduan"></span></td>
						</tr>
						<tr>
							<td>Tanggal & Waktu</td>
							<td>:</td>
							<td><span class="detil-tanggal_waktu"></span></td>
						</tr>
						<tr>
							<td>Lokasi</td>
							<td>:</td>
							<td><span class="detil-lokasi"></span></td>
						</tr>
						<tr>
							<td>Desa</td>
							<td>:</td>
							<td><span class="detil-desa"></span></td>
						</tr>
						<tr>
							<td>Status</td>
							<td>:</td>
							<td><span class="detil-status"></span></td>
						</tr>
					</table>

					<h5>Riwayat Pengaduan</h5>
					<table class="table table-striped list-riwayat-pengaduan">
						<thead>
							<tr>
								<td>No</td>
								<td>Keterangan</td>
								<td>Tanggal dibuat</td>
								<td>Pembuat</td>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
					<div class="row">
						<div class="col-md-12">
							<div class="card bg-blue card--inverse">
								<div class="card-body">
									<h4 class="card-title">Isi Pengaduan</h4>
									<p class="card-text detil-isi_pengaduan"></p>
								</div>
							</div>
						</div>
						<div class="col-md-1"></div>
						<div class="col-md-11">
							<div class="card bg-green card--inverse">
								<div class="card-body">
									<h4 class="card-title">Isi Tanggapan</h4>
									<p class="card-text detil-tanggapan_pengaduan"></p>
								</div>
							</div>	
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-tanggapi" tabindex="-1">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title pull-left">Tanggapi Pengaduan</h5>
				</div>
				<div class="modal-body">
					<input type="hidden" id="tanggapan_pengaduan_id">
					<div class="form-group">
						<label>Isi Tanggapan Pengaduan</label>
						<textarea class="form-control" rows="10" id="isi_tanggapan"></textarea>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-link button-tanggapi">Tanggapi</button>
					<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<?php endblock() ?>

<?php startblock('custom_js') ?>
	<?php superblock() ?>
	<script type="text/javascript">
		function verifikasi_finish(elemen){
			swal({
				title: 'Apakah Anda yakin?',
				text: "Verfikasi bahwa pengaduan telah selesai diproses!",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#68C39F',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Ya, Hapus data!'
			}).then((result) => {
				if (result) {
					$.ajax({
						url: elemen.data('url'),
						type: 'GET',
						success: function(respon){
							respon = JSON.parse(respon)
							if(respon.success == true){
								swal({
									title: 'Berhasil!',
									text: 'Data berhasil terverifikasi.',
									type: 'success',
									showConfirmButton: false,
									timer: 1500,
								})
							}else{
								swal({
									title: 'Error!',
									text: 'Terjadi kesalahan saat proses menghapus data.',
									type: 'error',
									showConfirmButton: false,
									timer: 1500,
								})
							}
							pagination_(getUrlParameter('page'))
						}
					})
				}
			})
		}

		var modal_tanggapi = function(elem){
			id_pengaduan = elem.data('pengaduanid');
			$('#modal-tanggapi').modal('show')
			$('#tanggapan_pengaduan_id').val(id_pengaduan);
		}

		$('.button-tanggapi').on('click', function(){
			$.ajax({
				type : "POST",
				data : {'id_pengaduan' : $('#tanggapan_pengaduan_id').val(), 'isi_tanggapan': $('#isi_tanggapan').val()},
				url : "<?= base_url() ?>admin/pengaduan/actiontanggapi",
				success: function(respon){
					respon = JSON.parse(respon)
					if(respon.success){
						swal({
							title: 'Berhasil!',
							text: 'Data berhasil tersimpan.',
							type: 'success',
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							$('#modal-tanggapi').modal('hide')
							window.location.reload();
						},2000);
					}
				}
			})
		})

		function get_pengaduan_map(obj){
			url = obj.data('url');
			if(url){
				$.ajax({
					url: url,
					type: "GET",
					success: function(respon_){
						respon = JSON.parse(respon_);
						if(respon){
							if (respon.lt && respon.lg) {
								var myLatLng = {lat: Number(respon.lg), lng: Number(respon.lt)};
								var mapProp = new google.maps.Map(document.getElementById('googleMap'), {
									zoom: 12,
									center: myLatLng
								});
								var marker = new google.maps.Marker({
									position: myLatLng,
									map: mapProp,
									title: 'Hello World!'
								});
								$('#modal-pengaduan_map').modal('show')
							}
						}
					}
				});
			}
		}

		function detail_pengaduan(obj){
			url = obj.data('url');
			if(url){
				$.ajax({
					url: url,
					type: "GET",
					success: function(respon_){
						respon = JSON.parse(respon_);
						if(respon){
							$('.detil-nama').text(respon.nama);
							$('.detil-jenis_pengaduan').text(respon.nama_jenis);
							$('.detil-tanggal_waktu').text(respon.waktu);
							$('.detil-lokasi').text(respon.alamat_lokasi);
							$('.detil-desa').text(respon.nama_desa);
							$('.detil-status').text(respon.nama_status);
							$('.detil-isi_pengaduan').text(respon.isi_pengaduan);
							$('.detil-tanggapan_pengaduan').text(respon.tanggapan);

							$.ajax({
								url: "<?= base_url() ?>admin/riwayat/show?pengaduan_id="+respon.id,
								type: "GET",
								success: function(riwayat_){
									riwayat = JSON.parse(riwayat_);
									for(var i = 0; i < riwayat.objects.length; i++){
										no = i+1
										rowtable = '<tr>'+
												'<td>'+no+'</td>'+
												'<td>'+riwayat.objects[i].keterangan+'</td>'+
												'<td>'+riwayat.objects[i].tanggal_dibuat+'</td>'+
												'<td>'+riwayat.objects[i].nama+'</td>'+
												'</tr>'
										$('.list-riwayat-pengaduan > tbody').append(rowtable);
									}
								}
							});
							$('#modal-default').modal('show')
						}
					}
				});
			}
		}

		function pagination_(current, action){
			this_page = parseInt(current)+1
			url_ = pathname+"/show";
			if(this_page){
				offset = current*10;
				url_ = pathname+"/show?limit=100&start="+offset+"&q="+getUrlParameter('q')+"&p="+getUrlParameter('p');
			}
			$.ajax({
				url: url_,
				type: "GET",
				success: function(respon){
					changeUrl("page", current);
					page_ = parseInt(getUrlParameter("page"))
					respon = JSON.parse(respon)
					total_count = parseInt(respon.meta.total_count)
					total_count_page = parseInt(respon.meta.total_count_page)
					limit = parseInt(respon.meta.limit)
					total_pagination = respon.meta.total_pagination
					start = parseInt(respon.meta.start)
					$(".change-list > tbody").html("")
					if(respon.objects.length > 0){
						for (var i = 0; i < respon.objects.length; i++){
							no = start+1+i

							isi_pengaduan = '<div class="list-word-break" style="max-height: 100px; max-width:50px">'+
								'<p>'+respon.objects[i].isi_pengaduan+'</p>'+
								'</div>';

							tanggapan = '<div class="list-word-break" style="max-height: 100px;max-width:50px">'+
								'<p>'+respon.objects[i].tanggapan+'</p>'+
								'</div>';


							button_map = '';
							lt_ = respon.objects[i].lt
							lg_ = respon.objects[i].lg
							if (lt_ && lg_ ) {
								if (lt_ != null && lg_ != null) {
									button_map = '<a onclick="get_pengaduan_map($(this))" data-url="<?= base_url() ?>admin/pengaduan/show/?id='+respon.objects[i].id+'" href="javascript:void(0);" class="dropdown-item">Map Lokasi</a>'
								}
							}

							button_verifikasi = ''
							status_badge = ''
							if(respon.objects[i].status_id == 1){
								<?php if(!in_array($user_saya->grup_id, $user_selain_pegawai)) { ?>
									button_verifikasi = '<a onclick="modal_tanggapi($(this))" data-pengaduanid="'+respon.objects[i].id+'" href="javascript:void(0);" class="dropdown-item">Tanggapi</a>'
								<?php } ?>
								status_badge = '<span class="badge badge-danger">'+respon.objects[i].nama_status+'</span>'
							}else if(respon.objects[i].status_id == 2){
								<?php if($user_saya->grup_id == 7) { ?>
									button_verifikasi = '<a data-url="<?= base_url() ?>admin/pengaduan/varifikasifinish/'+respon.objects[i].id+'" onclick="verifikasi_finish($(this))" data-pengaduanid="'+respon.objects[i].id+'" href="javascript:void(0);" class="dropdown-item">Verifikasi Finish</a>'
								<?php } ?>
								status_badge = '<span class="badge badge-warning">'+respon.objects[i].nama_status+'</span>'
							}else if(respon.objects[i].status_id == 3){
								status_badge = '<span class="badge badge-success">'+respon.objects[i].nama_status+'</span>'
							}

							action_button = '<div class="dropdown-demo">'+
												'<div class="dropdown">'+
													'<button class="btn btn-primary" data-toggle="dropdown">Action</button>'+
													'<div class="dropdown-menu">'+
														'<a href="javascript:void(0);" class="dropdown-item" onclick="detail_pengaduan($(this))" data-url="<?= base_url() ?>admin/pengaduan/show/?id='+respon.objects[i].id+'">Detail Pengaduan</a>'+
														button_map+
														button_verifikasi+
													'</div>'+
												'</div>'+
											'</div>';


							row_ = '<tr>'+
										'<th scope="row">'+no+'</th>'+
										'<td>'+respon.objects[i].nama+'</td>'+
										'<td>'+respon.objects[i].waktu+'</td>'+
										// '<td>'+respon.objects[i].alamat_lokasi+'</td>'+
										'<td>'+isi_pengaduan+'</td>'+
										'<td>'+tanggapan+'</td>'+
										'<td>'+respon.objects[i].nama_jenis+'</td>'+
										// '<td>'+respon.objects[i].nama_desa+'</td>'+
										'<td>'+status_badge+'</td>'+
										"<td>"+
											action_button+
										"</td>"+
									'</tr>'
							$(".change-list > tbody").append(row_);
						}
						$('.list-word-break').dotdotdot();
					}else{
						row_ = "<tr>"+
						  "<td align='center' colspan='10'>Kosong / tidak ada data</td>"+
						  "</tr>";
						$(".change-list > tbody").append(row_)
					}
				}
			})
		}
	</script>
<?php endblock() ?>