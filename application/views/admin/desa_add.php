<?php include('change_form.php') ?>

<?php startblock('change_form')?>
	<form method="POST" action="<?= base_url() ?><?= $action ?>" id="change_form">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title">Add <?= $title ?></h2>

				<div class="form-group">
					<label>Nama Desa</label>
					<input type="text" class="form-control" placeholder="" name="nama_desa">
					<i class="form-group__bar"></i>
				</div>

			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>admin/desa" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>
<?php endblock() ?>