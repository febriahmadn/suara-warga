<?php include('change_form.php') ?>

<?php startblock('change_form')?>
	<form method="POST" action="<?= base_url() ?><?= $action ?>" id="change_form">
		<input type="hidden" name="id" value="<?= $_id ?>">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title"><?= $title ?></h2>
				<div class="form-group">
					<label>Password Baru</label>
					<input type="password" class="form-control" placeholder="" name="password">
					<i class="form-group__bar"></i>
				</div>
			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>admin/akun" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>
<?php endblock() ?>