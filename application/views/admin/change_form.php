<?php include('base.php') ?>

<?php startblock('title') ?>
	<?= $title ?>
<?php endblock() ?>

<?php startblock('css') ?>
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/css/select2.min.css">
	<link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/flatpickr/flatpickr.min.css" />
<?php endblock() ?>

<?php startblock('js') ?>
	<script src="<?= base_url() ?>static/js/jquery.form.min.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/bower_components/select2/dist/js/select2.full.min.js"></script>
	<script src="<?= base_url() ?>static/admin/vendors/flatpickr/flatpickr.min.js"></script>
	<script src="<?= base_url() ?>static/js/bootstrap-datepicker.js"></script>
<?php endblock() ?>

<?php startblock('custom_css') ?>
	<style type="text/css">
		.form-control{
			border: 1px solid #b7b6b6;
		}

		.select2.select2-container.select2-container--default {
			border: 1px solid #e3e3e3;
			padding-right: 5px;
			padding-left: 5px;
			border-radius: 3px;
		}

		.date-picker, .datepicker, input[type="text"]{
			border: 1px solid #C1C1C1;
			border-radius: 5px;
			padding-left: 5px;
		}
		.datepicker.datepicker-dropdown{
			width: 280px;
		}
		div.datepicker thead tr:first-child th {
			cursor: pointer;
			padding: 8px 0px;
			text-align: center;
		}
		div.datepicker table {
			text-align: center;
			width: 100%;
			margin: 0;
		}
		div.datepicker thead tr:first-child th:hover {
			background: #F5F5F5;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;
		}
		div.datepicker td span {
			display: block;
			width: 31%;
			height: 54px;
			line-height: 54px;
			float: left;
			margin: 2px;
			cursor: pointer;
		}
		div.datepicker td span:hover {
			background: #F5F5F5;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;
		}
		div.datepicker td span.active {
			background: #33414e;
			color: #fff;
			-moz-border-radius: 3px;
			-webkit-border-radius: 3px;
			border-radius: 3px;
		}
	</style>
<?php endblock() ?>

<?php startblock('header') ?>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?= base_url() ?>admin">Home</a></li>
		<li class="breadcrumb-item">Master</li>
		<li class="breadcrumb-item"><?= $title ?></li>
		<li class="breadcrumb-item">Add</li>
	</ol>
<?php endblock() ?>

<?php startblock('header1') ?>
	<ol class="breadcrumb1">
		<li class="breadcrumb1-item"><a href="<?= base_url() ?>admin">Home</a></li>
		<li class="breadcrumb1-item">Akun</li>
		<li class="breadcrumb1-item"><?= $title ?></li>
		<li class="breadcrumb1-item">Add</li>
	</ol>
<?php endblock() ?>

<?php startblock('isi') ?>
	<?php startblock("alert") ?>
		<div class="change-form-message"></div>
	<?php endblock() ?>
	<?php startblock('change_form')?>
	<form method="POST" action="" id="change_form">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title">Add <?= $title ?></h2>
				<div class="form-group">
					<label>Nama Desa</label>
					<input type="text" class="form-control" placeholder="">
					<i class="form-group__bar"></i>
				</div>
			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<button type="button" class="btn btn-danger">Batal</button>
			</div>
		</div>
	</form>
	<?php endblock() ?>
<?php endblock() ?>

<?php startblock('custom_js') ?>
	<script type="text/javascript">
		$(".datepicker").datepicker({ format: 'yyyy-mm-dd'})
		
		var button_loading = function(element, type){
			if(type == "disabled"){
				text_ori = element.html()
				element.attr("data-text", text_ori)
				element.attr("disabled", true)
				element.text("Loading")
			}else{
				element.attr("disabled", false)
				element.html(element.attr("data-text"))
			}
		}

		$('.button-submit').on('click', function(){
			form = $("#change_form")
			$this = $(this)
			button_loading($this, "disabled")
			form.ajaxSubmit({
				url: form.attr("action"),
				type: "POST",
				data: form.serialize(),
				success: function(respon){
					button_loading($this, "show")
					respon = JSON.parse(respon)
					if(respon.success == false){
						error_html = '<div class="alert alert-danger" role="alert">'+
									'<h4 class="alert-heading">Error!</h4>'+
									respon.error+
									'</div>';
						$('.change-form-message').html(error_html);
						window.scrollTo(0,0)
					}else{
						swal({
							title: 'Berhasil!',
							text: 'Data berhasil tersimpan.',
							type: 'success',
							showConfirmButton: false,
							timer: 1500,
						});
						setTimeout(function () {
							window.location.href = "<?= base_url() ?>"+respon.url;
						},2000);
					}
				},
				error: function(){
					button_loading($this, "show")
					console.log("error")
				}
			})
		});

		var select_option_ = function(url, title, elem, value){
			$.ajax({
				url: url,
				type: "GET",
				success: function(respon){
					respon = JSON.parse(respon);
					if(respon.length  > 0){
						row_ = '<option value>-- Pilih '+title+' --</option>';
						for (var i = 0; i < respon.length; i++){
							row_ += '<option value="'+respon[i].id+'">'+respon[i].name+'</option>';
						}
						$("."+elem+"-select").html(row_).trigger('change.select2');
						if(value != undefined){
							$("."+elem+"-select").val(value).trigger('change');
						}
					}
				}
			});
		}
	</script>
<?php endblock() ?>