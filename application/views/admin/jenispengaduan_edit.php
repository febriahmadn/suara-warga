<?php include('change_form.php') ?>

<?php startblock('change_form')?>
	<form method="POST" action="<?= base_url() ?><?= $action ?>" id="change_form">
		<input type="hidden" name="id" value="<?= $_id ?>">
		<div class="card">
			<div class="card-body">
				<h2 class="card-title">Edit <?= $title ?></h2>

				<div class="form-group">
					<label>Jenis Pengaduan</label>
					<input type="text" class="form-control" placeholder="" name="nama_jenis">
					<i class="form-group__bar"></i>
				</div>

				<div class="form-group">
					<label>Keterangan</label>
					<input type="text" class="form-control" placeholder="" name="keterangan_jenis">
					<i class="form-group__bar"></i>
				</div>

			</div>
			<div class="card-footer">
				<button type="button" class="btn btn-success button-submit">Simpan</button>
				<a href="<?= base_url() ?>admin/desa" type="button" class="btn btn-danger">Batal</a>
			</div>
		</div>
	</form>
<?php endblock() ?>

<?php startblock("custom_js") ?>
	<?php superblock() ?>
	<script type="text/javascript">
		$.ajax({
			url: "<?= base_url() ?>/admin/jenispengaduan/show/?id=<?php echo $_id?>",
			type: "GET",
			success: function(respon){
				respon = JSON.parse(respon);
				if(respon){
					$("input[name='nama_jenis']").val(respon.nama_jenis)
					$("input[name='keterangan_jenis']").val(respon.keterangan_jenis)
					// if(respon.keterangan != null && respon.keterangan != ""){
					// 	$("textarea[name='keterangan']").val(respon.keterangan)
					// }
				}
			}
		})
	</script>
<?php endblock() ?>