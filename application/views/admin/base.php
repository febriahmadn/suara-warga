<?php
define('__ROOT__',dirname(dirname(__FILE__)));
require_once(__ROOT__.'/ti.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.css">        
        <?php emptyblock('css') ?>
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/css/app.min.css">
        <?php emptyblock('custom_css') ?>
        <title><?php emptyblock('title') ?> | PM (Pengaduan Masyarakat)</title>
        <?php $user_saya = $this->session->userdata('result') ?>
        <?php $user_selain_pegawai =  array(4, 8, 7) ?>
        <script type="text/javascript">
            var pathname = window.location.pathname;
        </script>
        
    </head>

    <body data-ma-theme="blue">
        <main class="main">
            <div class="page-loader">
                <div class="page-loader__spinner">
                    <svg viewBox="25 25 50 50">
                        <circle cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
                    </svg>
                </div>
            </div>

            <header class="header">
                <div class="navigation-trigger hidden-xl-up" data-ma-action="aside-open" data-ma-target=".sidebar">
                    <div class="navigation-trigger__inner">
                        <i class="navigation-trigger__line"></i>
                        <i class="navigation-trigger__line"></i>
                        <i class="navigation-trigger__line"></i>
                    </div>
                </div>

                <div class="header__logo hidden-sm-down">
                    <h1><a href="<?= base_url() ?>admin">Pengaduan Masyarakat</a></h1>
                </div>

                <ul class="top-nav">
                    <li class="hidden-xl-up"><a href="#" data-ma-action="search-open"><i class="zmdi zmdi-search"></i></a></li>

                    <li class="dropdown hidden-xs-down">
                        <a href="#" data-toggle="dropdown"><i class="zmdi zmdi-more-vert"></i></a>

                        <div class="dropdown-menu dropdown-menu-right">
                            <a href="#" class="dropdown-item">Logout</a>
                            <a href="#" class="dropdown-item">Clear Local Storage</a>
                        </div>
                    </li>
                </ul>
            </header>

            <aside class="sidebar">
                <div class="scrollbar-inner">
                    <div class="user">
                        <div class="user__info" data-toggle="dropdown">
                            <img class="user__img" src="<?= base_url() ?>static/admin/demo/img/profile-pics/8.jpg" alt="">
                            <div>
                                <div class="user__name"><?php if($user_saya->nama) echo $user_saya->nama ?></div>
                                <div class="user__email"><?php if($user_saya->email) echo $user_saya->email ?></div>
                            </div>
                        </div>

                        <div class="dropdown-menu">
                           <!--  <a class="dropdown-item" href="#">View Profile</a>
                            <a class="dropdown-item" href="#">Settings</a> -->
                            <a class="dropdown-item" href="<?= base_url() ?>admin/akun/edit_profil">Edit Profil</a>
                            <a class="dropdown-item" href="<?= base_url() ?>admin/login/logout">Logout</a>
                        </div>
                    </div>

                    <?php include('menus.php') ?>
                </div>
            </aside>

            <section class="content">
                <?php emptyblock('header') ?>
                <?php emptyblock('isi') ?>
            </section>
        </main>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery.scrollbar/jquery.scrollbar.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery-scrollLock/jquery-scrollLock.min.js"></script>
        <script src="<?= base_url() ?>static/admin/js/app.min.js"></script>
        <?php emptyblock('js') ?>
        <script type="text/javascript">
            function getUrlParameter(sParam) {
                var sPageURL = decodeURIComponent(window.location.search.substring(1)),
                sURLVariables = sPageURL.split('&'),
                sParameterName,
                i;

                for (i = 0; i < sURLVariables.length; i++) {
                    sParameterName = sURLVariables[i].split('=');

                    if (sParameterName[0] === sParam) {
                        return sParameterName[1] === undefined ? true : sParameterName[1];
                    }
                }
            };


            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            function changeUrl(key,value) {
                //Get query string value
                var searchUrl=location.search;
                if(searchUrl.indexOf("?")== "-1") {
                    var urlValue='?'+key+'='+value;
                    history.pushState({state:1, rand: Math.random()}, '', urlValue);
                }else{
                    //Check for key in query string, if not present
                    if(searchUrl.indexOf(key)== "-1") {
                        var urlValue=searchUrl+'&'+key+'='+value;
                    }
                    else {  //If key present in query string
                        oldValue = getParameterByName(key);
                        if(searchUrl.indexOf("?"+key+"=")!= "-1") {
                            urlValue = searchUrl.replace('?'+key+'='+oldValue,'?'+key+'='+value);
                        }
                        else {
                            urlValue = searchUrl.replace('&'+key+'='+oldValue,'&'+key+'='+value); 
                        }
                    }
                    history.pushState({state:1, rand: Math.random()}, '', urlValue);
                }
            }
        </script>
        <?php emptyblock('custom_js') ?>
    </body>
</html>