<?php include('change_list.php') ?>

<?php startblock('header') ?>
	<ol class="breadcrumb">
		<li class="breadcrumb-item"><a href="<?= base_url() ?>admin">Home</a></li>
		<li class="breadcrumb-item">Master</li>
		<li class="breadcrumb-item active"><?= $title ?></li>
	</ol>
<?php endblock() ?>

<?php startblock('change_list') ?>
	<table class="table table-striped mb-0 change-list">
		<thead>
			<tr>
				<th>Nomor</th>
				<th>Jenis Pengaduan</th>
				<th>Keterangan</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
<?php endblock() ?>

<?php startblock('custom_js') ?>
	<?php superblock() ?>
	<script type="text/javascript">
		function pagination_(current, action){
			this_page = parseInt(current)+1
			url_ = pathname+"/show";
			if(this_page){
				offset = current*10;
				url_ = pathname+"/show?limit=100&start="+offset+"&q="+getUrlParameter('q');
			}
			$.ajax({
				url: url_,
				type: "GET",
				success: function(respon){
					changeUrl("page", current);
					page_ = parseInt(getUrlParameter("page"))
					respon = JSON.parse(respon)
					total_count = parseInt(respon.meta.total_count)
					total_count_page = parseInt(respon.meta.total_count_page)
					limit = parseInt(respon.meta.limit)
					total_pagination = respon.meta.total_pagination
					start = parseInt(respon.meta.start)
					$(".change-list > tbody").html("")
					if(respon.objects.length > 0){
						for (var i = 0; i < respon.objects.length; i++){
							no = start+1+i
							row_ = '<tr>'+
										'<th scope="row">'+no+'</th>'+
										'<td>'+respon.objects[i].nama_jenis+'</td>'+
										'<td>'+respon.objects[i].keterangan_jenis+'</td>'+
										"<td>"+
											'<a href="'+pathname+'/edit/'+respon.objects[i].id+'"><button class="btn btn-primary btn--icon-text"><i class="zmdi zmdi-edit"></i> Edit</button></a>'+
											'&nbsp'+
											'<button onclick="delete_($(this))" data-url="'+pathname+'/delete/?id='+respon.objects[i].id+'" class="btn btn-danger btn--icon-text"><i class="zmdi zmdi-delete"></i> Delete</button>'+
										"</td>"+
									'</tr>'
							$(".change-list > tbody").append(row_)
						}
					}else{
						row_ = "<tr>"+
						  "<td align='center' colspan='10'>Kosong / tidak ada data</td>"+
						  "</tr>";
						$(".change-list > tbody").append(row_)
					}
				}
			})
		}
	</script>
<?php endblock() ?>