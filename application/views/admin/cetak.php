<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8' />
	<title>Cetak Pengaduan</title>
	<style type="text/css">
		table, td, th {
		  border: 1px solid black;
		}

		table {
		  border-collapse: collapse;
		  width: 100%;
		}

		th {
		  height: 50px;
		}
	</style>
</head>
<body>
	<h2>Cetak Pengaduan</h2>
	<table border="1">
		<thead>
			<tr>
				<td>No</td>
				<td>Nama Pengadu</td>
				<td>Jenis Pengaduan</td>
				<td>Waktu</td>
				<td>Alamat Lokasi</td>
				<td>Isi</td>
				<td>Tanggapan</td>
				<td>Status</td>
			</tr>
		</thead>
		<tbody>

			<?php $index = 1; foreach ($result as $p) { ?>
				<tr>
					<td><?= $index ?></td>
					<td><?php echo $p->nama ?></td>
					<td><?php echo $p->nama_jenis ?></td>
					<td><?php echo $p->waktu ?></td>
					<td><?php echo $p->alamat_lokasi ?></td>
					<td><?php echo $p->isi_pengaduan ?></td>
					<td><?php echo $p->tanggapan ?></td>
					<td><?php echo $p->nama_status ?></td>
				</tr>
			<?php $index++; } ?>
		</tbody>
	</table>
</body>
</html>