<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/material-design-iconic-font/dist/css/material-design-iconic-font.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/vendors/bower_components/animate.css/animate.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.css">
        <link rel="stylesheet" href="<?= base_url() ?>static/admin/css/app.min.css">
        <style type="text/css">
            .form-control{
                border: 1px solid #b7b6b6;
                border-radius: 5px;
            }

            .datepicker.datepicker-dropdown{
                width: 280px;
            }

            div.datepicker thead tr:first-child th {
                cursor: pointer;
                padding: 8px 0px;
                text-align: center;
            }
            div.datepicker table {
                text-align: center;
                width: 100%;
                margin: 0;
            }
            div.datepicker thead tr:first-child th:hover {
                background: #F5F5F5;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }
            div.datepicker td span {
                display: block;
                width: 31%;
                height: 54px;
                line-height: 54px;
                float: left;
                margin: 2px;
                cursor: pointer;
            }
            div.datepicker td span:hover {
                background: #F5F5F5;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }
            div.datepicker td span.active {
                background: #33414e;
                color: #fff;
                -moz-border-radius: 3px;
                -webkit-border-radius: 3px;
                border-radius: 3px;
            }
            .login__block {
                max-width: 500px;
            }
        </style>
    </head>

    <body data-ma-theme="green">
        <form method="POST" action="<?= base_url() ?>daftaraction" id="change_form" onsubmit="return false">
            <input type="hidden" name="grup_id" value="4">
            <div class="login">
                <div class="login__block active" id="l-login">
                    <div class="login__block__header">
                        <i class="zmdi zmdi-account-circle"></i>
                        Hi there! Please Sign in
                    </div>

                    <div class="login__block__body">
                        <div class="change-form-message"></div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-group--centered">
                                    <label>Nama Lengkap</label>
                                    <input type="text" class="form-control" name="nama">
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group form-group--centered">
                                    <label>NIK</label>
                                    <input type="text" class="form-control" name="nik">
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group form-group--centered">
                                    <label>Email Address</label>
                                    <input type="text" class="form-control" name="email">
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group" style="text-align: left;">
                                    <label>Jenis Kelamin</label>

                                    <div class="custom-control custom-radio">
                                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="L">
                                        <label class="custom-control-label" for="customRadio1">Laki-Laki</label>
                                    </div>
                                    <div class="clearfix mb-2"></div>
                                    <div class="custom-control custom-radio">
                                        <input type="radio" name="jenis_kelamin" class="custom-control-input" value="P">
                                        <label class="custom-control-label" for="customRadio1">Perempuan</label>
                                    </div>
                                    <div class="clearfix mb-2"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group form-group--centered">
                                    <label>Password</label>
                                    <input type="password" class="form-control" name="password">
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group form-group--centered">
                                    <label>Tempat Lahir</label>
                                    <input type="text" class="form-control" name="tempat_lahir">
                                    <i class="form-group__bar"></i>
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Lahir</label>
                                    <input type="text" class="form-control datepicker" name="tanggal_lahir">
                                    <i class="form-group__bar"></i>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-success button-submit">Daftar</button>
                    </div>
                </div>
            </div>
        </form>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/jquery/dist/jquery.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/popper.js/dist/umd/popper.min.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="<?= base_url() ?>static/js/jquery.form.min.js"></script>
        <script src="<?= base_url() ?>static/js/bootstrap-datepicker.js"></script>
        <script src="<?= base_url() ?>static/admin/vendors/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
        <script src="<?= base_url() ?>static/admin/js/app.min.js"></script>        
        <script type="text/javascript">
            

            $(".datepicker").datepicker({ format: 'yyyy-mm-dd'})
            $('.button-submit').on('click', function(){
                form = $("#change_form")
                form.ajaxSubmit({
                    url: form.attr("action"),
                    type: "POST",
                    data: form.serialize(),
                    success: function(respon){
                        respon = JSON.parse(respon);
                        if(respon.success == true){
                            swal({
                                title: 'Berhasil!',
                                text: 'Data berhasil tersimpan.',
                                type: 'success',
                                showConfirmButton: false,
                                timer: 1500,
                            });
                            setTimeout(function () {
                                window.location.href = "<?= base_url() ?>";
                            },2000);
                        }else{
                            error_html = '<div class="alert alert-danger">'+
                                        '<h4 class="alert-heading">Terjadi Kesalahan</h4>'+
                                        respon.error+
                                        '</div>';
                            $('.change-form-message').html(error_html);
                        }
                    }
                });
            });
        </script>
    </body>
</html>