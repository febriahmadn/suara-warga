<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_riwayat extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function getlist($limit, $start, $st = NULL, $id, $pengaduan_id){
		if ($st == NULL) $st = "";
		if(!$limit){
			$limit = 0;
		}
		if(!$start){
			$start = 0;
		}
		$limit = $limit;

		if($start > 0){
			$limit = $start.",".$limit;
		}
		
		$this->db->select('riwayat.id, riwayat.keterangan, riwayat.tanggal_dibuat, akun.nama');
		$this->db->from('riwayat');
		$this->db->join('akun', 'riwayat.created_by=akun.id', 'left');
		$this->db->limit($limit, $start);
		if($id != NULL){
			$this->db->where('id', $id);
		}
		if($pengaduan_id != NULL)
			$this->db->where('pengaduan_id', $pengaduan_id);

		$query = $this->db->get();
		return $query->result();
	}

	function getAll($pengaduan_id){
		$this->db->select('riwayat.id, riwayat.keterangan, riwayat.tanggal_dibuat, akun.nama');
		$this->db->from('riwayat');
		$this->db->join('akun', 'riwayat.created_by=akun.id', 'left');
		if($pengaduan_id != NULL)
			$this->db->where('pengaduan_id', $pengaduan_id);
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($st = NULL){
		if ($st == NULL) $st = "";
		$sql = "select * from riwayat";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}
}