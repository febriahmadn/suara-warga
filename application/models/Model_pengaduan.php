<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pengaduan extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	public function add($file, $akun_id){
		$field = array(
			'waktu' => date('Y-m-d h:i:s'),
			'lg' => $this->input->post('lg'),
			'lt' => $this->input->post('lt'),
			'alamat_lokasi' => $this->input->post('alamat_lokasi'),
			'jenispengaduan_id' => $this->input->post('jenispengaduan_id'),
			'desa_id' => $this->input->post('desa_id'),
			'akun_id' => $akun_id,
			'isi_pengaduan' => $this->input->post('isi_pengaduan'),
			'tanggapan' => "",
			'status_id' => 1,
			'photo' => $file,
		);
		$this->db->insert('pengaduan', $field);
		$insert_id = $this->db->insert_id();
		if($this->db->affected_rows() > 0){
			// create riwayat ambil dari id pengaduan
			$field = array(
				'keterangan' => "Pengaduan dibuat.",
				'pengaduan_id' => $insert_id,
				'tanggal_dibuat' => date('Y-m-d h:i:s'),
				'created_by' => $akun_id,
			);
			$this->db->insert('riwayat', $field);
			return true;
		}else{
			return false;
		}
	}

	function getlist($limit, $start, $st = NULL, $id, $p){

		$userku = $this->session->userdata('result'); // ambil data user session
		if ($st == NULL) $st = "";
		if(!$limit){
			$limit = 0;
		}
		if(!$start){
			$start = 0;
		}
		$limit = $limit;

		if($start > 0){
			$limit = $start.",".$limit;
		}
		
		$this->db->select('pengaduan.photo, pengaduan.id, pengaduan.waktu, pengaduan.lg, pengaduan.lt, pengaduan.alamat_lokasi, pengaduan.isi_pengaduan, pengaduan.tanggapan, pengaduan.jenispengaduan_id, pengaduan.desa_id, pengaduan.akun_id, pengaduan.status_id, jenis_pengaduan.nama_jenis, desa.nama_desa, akun.nama, status.nama_status');
		$this->db->from('pengaduan');
		$this->db->join('jenis_pengaduan', 'pengaduan.jenispengaduan_id=jenis_pengaduan.id', 'left');
		$this->db->join('desa', 'pengaduan.desa_id=desa.id', 'left');
		$this->db->join('akun', 'pengaduan.akun_id=akun.id', 'left');
		$this->db->join('status', 'pengaduan.status_id=status.id', 'left');
		$this->db->limit($limit, $start);

		if($p != NULL){
			if($p == 'sudah-tertangani'){
				$this->db->where('pengaduan.status_id', 3);
				// $this->db->or_where('pengaduan.status_id', 2);

			}elseif($p == 'verifikasi'){
				$this->db->where('pengaduan.status_id', 2);
			}elseif ($p == 'belum-tertangani') {
				$this->db->where('pengaduan.status_id', 1);
			}elseif ($p == 'archive') {
				$this->db->where('pengaduan.status_id', 4);
			}
		}

		if(isset($userku))
			if($userku->grup_id)
				if($userku->grup_id != 8)
					if($userku->grup_id == 2){
						// admin sarana prasarana
						$this->db->where('pengaduan.jenispengaduan_id', 8);
					}elseif($userku->grup_id == 3){
						//insfrastuktur
						$this->db->where('pengaduan.jenispengaduan_id', 6);
					}elseif($userku->grup_id == 6){
						//pelayanan
						$this->db->where('pengaduan.jenispengaduan_id', 9);
					}elseif($userku->grup_id == 5){
						//kesehatan
						$this->db->where('pengaduan.jenispengaduan_id', 3);
					}
		

		if($st){
			$st = strtolower($st);
			$st = str_replace('-', ' ', $st);
			// untuk mancari berdasarkan field yang di inginkan
			$this->db->like('lower(jenis_pengaduan.nama_jenis)', $st, 'both');
			$this->db->or_like('lower(desa.nama_desa)', $st, 'both');
			// $this->db->like('akun.nik', $st);
		}

		if($id != NULL){
			$this->db->where('pengaduan.id', $id);
		}
		$query = $this->db->get();
		return $query->result();
	}

	function getAll(){
		$this->db->select('pengaduan.photo, pengaduan.id, pengaduan.waktu, pengaduan.lg, pengaduan.lt, pengaduan.alamat_lokasi, pengaduan.isi_pengaduan, pengaduan.tanggapan, pengaduan.jenispengaduan_id, pengaduan.desa_id, pengaduan.akun_id, pengaduan.status_id, jenis_pengaduan.nama_jenis, desa.nama_desa, akun.nama, status.nama_status');
		
		$this->db->from('pengaduan');
		$this->db->join('jenis_pengaduan', 'pengaduan.jenispengaduan_id=jenis_pengaduan.id', 'left');
		$this->db->join('desa', 'pengaduan.desa_id=desa.id', 'left');
		$this->db->join('akun', 'pengaduan.akun_id=akun.id', 'left');
		$this->db->join('status', 'pengaduan.status_id=status.id', 'left');
		$query = $this->db->get();
		return $query->result();
	}

	function get_count($st = NULL){
		if ($st == NULL) $st = "";
		$sql = "select * from pengaduan";
		$query = $this->db->query($sql);
		return $query->num_rows();
	}

	function simpan_tanggapi($akun_id){
		$id = $this->input->post('id_pengaduan');
		$field = array(
			'tanggapan' => $this->input->post('isi_tanggapan'),
			'status_id' => 2,
			);
		$this->db->where('id', $id);
		$this->db->update('pengaduan', $field);

		if($this->db->affected_rows() > 0){
			$field = array(
				'keterangan' => "Pengaduan diproses.",
				'pengaduan_id' => $id,
				'tanggal_dibuat' => date('Y-m-d h:i:s'),
				'created_by' => $akun_id,
			);
			$this->db->insert('riwayat', $field);
			return true;
		}else{
			return false;
		}
	}

	function varifikasifinish($id, $akun_id){
		$field = array(
			'status_id' => 3,
			);
		$this->db->where('id', $id);
		$this->db->update('pengaduan', $field);

		if($this->db->affected_rows() > 0){
			$field = array(
				'keterangan' => "Pengaduan selesai diproses.",
				'pengaduan_id' => $id,
				'tanggal_dibuat' => date('Y-m-d h:i:s'),
				'created_by' => $akun_id,
			);
			$this->db->insert('riwayat', $field);
			return true;
		}else{
			return false;
		}
    }

    function jumlahpengaduanbulanan(){
    	$jenispengaduan_id = $this->input->get('jenispengaduan_id');
    	$bulan = $this->input->get('bulan');
    	$this->db->from('pengaduan');
    	$this->db->where('jenispengaduan_id', $jenispengaduan_id);
    	$this->db->where("EXTRACT(MONTH FROM waktu) = ", $bulan);
    	return $this->db->count_all_results();
    }
}